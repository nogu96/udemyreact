import React from 'react';
import WeatherIcons from 'react-weathericons';
import PropTypes from 'prop-types';


const WeatherTemperature = ({temperature, state}) => {
    return (
    <div className="weatherTemperatureContainer">
        <WeatherIcons name={state} size="4x" className="weathIcon"/>
        <span className="temperature">{`${temperature}`}</span>
        <span className="temperatureType">{`%`}</span>
    </div>)
};

WeatherTemperature.prototype = {
    temperature: PropTypes.number.isRequired,
    state: PropTypes.string.isRequired,
};

export default WeatherTemperature;