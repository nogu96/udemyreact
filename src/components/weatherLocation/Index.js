import React,{Component} from 'react';
import Location from './Location';
import WeatherData from './weatherData/Index';
import './styles.css';
import CircularProgress from '@material-ui/core/CircularProgress';

import ControllerAPI from '../../api/ControllerAPI.js';

class WeatherLocation extends Component {

    constructor() {
        super();    
        this.state = {
            city: "Buenos Aires",
            data: null,
        };
    }

    componentDidMount() {
        this.handleUpdateClick();
    }

    componentDidUpdate() {
        console.log("ComponentDidUpdate");
    }

    render() {
        console.log("render");
        const {city, data} = this.state
        return (
            <div className="weatherLocationCont">
                <Location city={city}/>
                {data ? 
                    <WeatherData data={data}/> : 
                    <CircularProgress/>
                }
            </div>
        );
    }

    handleUpdateClick = () => {
        ControllerAPI.getWeather("Buenos Aires,AR", response => {
            this.setState({
                data: response,
            });
        }, error => {
            
        });
    }

};

export default WeatherLocation; //hago que este disponible al mundo.