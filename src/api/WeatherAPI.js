const API_KEY = "&appid=a871b9e44dff86ff7101fe4ca12a4bf1";
const BASE_URL = "https://api.openweathermap.org/data/2.5/weather?";

function getCityWeather(city, success, failure) {
    const url = `${BASE_URL}q=${city}${API_KEY}`;
    fetch(url, {method: 'GET'}).then( resolve => {
        return resolve.json();
    }).then(json => {
        success(json);
    }).catch(error => {
        failure(error);
    });
}

export default {
    getCityWeather: getCityWeather,
}
