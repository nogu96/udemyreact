import WeatherAPI from './WeatherAPI.js';

const iconHash = {
    sunny: "day-sunny",
    fog: "day-fog",
    sun: "sun",
    rain: "rain",
    snow: "snow",
    windy: "windy",
    Clouds: "cloud",
    Clear: "day-sunny",
};

function getIconName(serverName) {
    debugger
    let iconName = iconHash[serverName];
    if(iconName == null) 
        iconName = "refesh";
    return iconName;
}

function parseKelvinToCelcious(kelvin) {
    return Math.round(kelvin -273,15);
}

function transformWeather(weatherData) {
    const { humidity, temp } = weatherData.main;
    const { speed } = weatherData.wind;
    const weatherState = getIconName(weatherData.weather[0].main);
    const temperature = parseKelvinToCelcious(temp);

    const data = {
        temperature,
        weatherState,
        humidity,
        wind: `${speed}m/s`,
    }
    debugger
    return data;
}


function getWeather(city, success, failure) {
    WeatherAPI.getCityWeather(city, (weatherData) => {
        const data = transformWeather(weatherData);
        success(data);
    }, () => {

    });
}

export default {
    getWeather: getWeather,
}
