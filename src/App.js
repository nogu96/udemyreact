import React from 'react';
import './App.css';
import WeatherData from './components/weatherLocation/Index';

function App() {
  return (
    <div className="App">
      <WeatherData/>
    </div>
  );
}

export default App;
